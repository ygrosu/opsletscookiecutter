In order to use the tool, provide the following:

REGION: the regions to connect to
ACCOUNT_NAME and ID are optional
SSH_KEY_FILE is the full path to the pem file you use (for now a single file)
SSH_USER the default user you connect by:  
ssh -i SSH_KEY_FILE SSH_USER@server  

TAG_NAME, TAG_ENV, TAG_ROLE are the tag names attatched to ec2 instances that you use when you want to filter the nodes by name/env/role.  

To activate - add the keys to your env (or ~/.bashrc)
export AWS_ACCESS_KEY_ID=-----  
export AWS_SECRET_ACCESS_KEY=-----  
  
then call:  
```  
{{cookiecutter.opslets_site}}/workstation/aws/shell/aws_ec2ls.py  
``` 
or better yet, alias them:

```  
alias ec2ls='{{cookiecutter.opslets_site}}/workstation/aws/shell/aws_ec2ls.py'   
alias ec2Prod='{{cookiecutter.opslets_site}}/workstation/aws/shell/aws_ec2ls.py production'   
```


