#!/usr/bin/env python


try:
    import sys, pip
except Exception as e:
    print "you need to install pip:  %s, please do so and restart"%e
    sys.exit(2)
try:
    pl = pip.get_installed_distributions()
    projects = [pr.project_name for pr in pl]
    needed_packages = ['boto', 'dateutil', 'os']
    for pk in needed_packages:
        if pk not in projects:
            raise SystemError("Package (%s) is missing."%pk)
except Exception as e:
    print "ec2ls setup problem.\n%s\nPlease fix and resume"%e
    sys.exit(2)

import argparse, sys, os
from datetime import datetime
from pprint import pprint
import boto, boto.ec2, boto.rds
import dateutil.parser


mandatory_keys= ['AWS_SECRET_ACCESS_KEY', 'AWS_ACCESS_KEY_ID']
missing_keys = filter(lambda s:os.getenv(s) is None, mandatory_keys)
if len(missing_keys) > 0:
    print "Some env keys are missing. (%s) Please export them."%missing_keys


ec2conn = boto.ec2.connect_to_region('{{REGION}}')

TAG_NAME='{{TAG_NAME}}'
TAG_ENV='{{TAG_ENV}}'
TAG_ROLE='{{TAG_ROLE}}'

key_path = os.getenv("SSH_KEY_FILE",'{{SSH_KEY_FILE}}')
username = os.getenv("SSH_USER_NAME", '{{SSH_USER}}')

def get_all_instances():
    instances = ec2conn.get_only_instances()
    result = list()
    for inst in instances:
        tag_name = inst.tags.get(TAG_NAME, "NO_NAME")
        tag_env = inst.tags.get(TAG_ENV, "NO_ENV")
        tag_role = inst.tags.get(TAG_ROLE, "NO_ROLE")
        
        the_ip = inst.ip_address 
        if the_ip is None:  #fallback from public to private IP
            the_ip= inst.private_ip_address
        result.append((inst.id, the_ip, tag_name, tag_env, inst.launch_time, inst.state, tag_role, inst.private_ip_address))
    return result


if __name__ == '__main__':
    
    the_instances = get_all_instances()
    
    filtered_env = sys.argv[1] if len(sys.argv) > 1 else None
    the_instances.sort(cmp=lambda x,y: cmp(x[4].lower(), y[4].lower()))
    
    commands = list()
    if len(the_instances) > 0:
        cmd_to_copy = None
        inst_to_copy = None
        print  "%-20s%-10s\t%s\t%-10s\t%-35s==> \t%s\n"%("Launch time", "Env (tag)", "Aws-Id   ", "State", "Name", "Command")
        for item in the_instances:
            if filtered_env is None or filtered_env.lower() in item[3].lower() or filtered_env.lower() in item[2].lower():
                cmdStr = "ssh -i %s %s@%s"%( key_path, username, item[1]) if item[5] == 'running' else ""
          
                ts = dateutil.parser.parse(item[4]) 
                launch_time = ts.strftime("%m-%d %H:%M:%S")

                print  "%-20s%-10s\t%s\t%-10s\t%-35s==> \t%s\n"%(launch_time, item[3], item[0], item[5], item[2], cmdStr)

                commands.append(cmdStr)
                commands.append(cmdStr)
                if item[5] == 'running':
                    cmd_to_copy = cmdStr
                    inst_to_copy = item[2]

        if filtered_env is not None and len(commands) >= 1 and cmd_to_copy is not None: 
            cmd = "echo %s | tr -d \"\n\" | pbcopy" % cmd_to_copy            
            os.system(cmd)            
            print "\n... put this in clipboard:  %s  ===> > >  %s "%(inst_to_copy, cmd_to_copy)


def new_inst():
    pass
