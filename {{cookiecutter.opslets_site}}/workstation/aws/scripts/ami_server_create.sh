#!/bin/bash

source $(dirname $0)/env_init.sh || exit 1
echo "Create basic AMI {{cookiecutter.opslets_site}}"

test -n "$CHEF_REPO_ROOT" || echo "Missing env variable CHEF_REPO_ROOT"
test -n "$CHEF_REPO_ROOT" || exit 1
pushd $CHEF_REPO_ROOT

ROLE=$1
ROLE_OPTIONS="base_ami/worker_inst"
test -n "${ROLE}" || echo "Chef Role missing options are: ${ROLE_OPTIONS}"
test -n "${ROLE}" || exit 1

echo "----"

case ${ROLE} in
  "base_ami" | "worker_inst") echo "Role is ok ${ROLE}";;
  
  *) echo  "unknown role"; echo " should be:  ${ROLE_OPTIONS}"; echo "exiting"; exit 1;;
esac
ENV=${2:-testing}
echo "----"
echo  "command is: $0 $1 : $@" 



UBU_BASE_AMI=ami-8ee605bd
SGS='default'

NOW=$(date +"%Y-%m-%d--%H-%M")
TEMPLATE_NAME=${3:-"{{cookiecutter.opslets_site}}-$NOW"}
IAM_PROFILE='buildGetter'
THE_ROLE=${ROLE}


rm deploy.log
echo "----"
echo "Env ${ENV}  NAME:${TEMPLATE_NAME}"

# CMD="knife ec2 server create -g ${SGS} -S ${SSH_KEY_NAME} --json-attributes "{\"RUN_ENV\":\"$ENV\"}" --iam-profile ${IAM_PROFILE} --ssh-user ubuntu -i ${SSH_PRIV_KEY} --region ${REGION} -I ${UBU_BASE_AMI} --flavor m4.large  -N ${TEMPLATE_NAME} -r 'role[${THE_ROLE}]' "
CMD="knife ec2 server create -g ${SGS} -S ${SSH_KEY_NAME} --iam-profile ${IAM_PROFILE} --ssh-user ubuntu -i ${SSH_PRIV_KEY} --region ${REGION} -I ${UBU_BASE_AMI} --flavor m4.large  -N ${TEMPLATE_NAME} -r 'role[${THE_ROLE}]' "


echo "Will execute ${CMD} "
echo "----"
echo -n "continue?"
read var_name_just_for_prompt
echo "----"
${CMD}



popd


