#!/bin/bash


SSH_PRIV_KEY="{{cookiecutter.SSH_KEY_FILE}}"
SSH_KEY_NAME="{{cookiecutter.SSH_KEY}}"
REGION='{{cookiecutter.REGION}}'

if [[ x"$AWS_ACCESS_KEY_ID" == x"" ]]
then
   echo "FIX_ME# NO AWS KEYs set"
   exit 1
fi
