#!/usr/bin/env python


import sys

ssh_key='{{ cookiecutter.SSH_KEY}}'

if len(ssh_key) < 2:
	print "ssh_key is too short: (%s)"%(ssh_key)
	sys.exit(1)


ssh_key_file='{{ cookiecutter.SSH_KEY_FILE}}'

import os.path
from os.path import expanduser
if not os.path.isfile(expanduser(ssh_key_file)):
	print "ssh_key_file does not exist: (%s)"%(expanduser(ssh_key_file))
	sys.exit(1)
